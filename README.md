# `ollama` on [cleps](https://sed.paris.inria.fr/cleps)

This repository provides the script
[`ollama_on_cleps.sh`](ollama_on_cleps.sh) that runs an `ollama`
server on a GPU node on cleps and redirects a local port (by
default, 11434, the [default port] for `ollama`) to this instance, so
that the `ollama` client can be used locally, targetting the server
running on cleps.

[default port]: https://github.com/ollama/ollama/blob/main/docs/faq.md#how-can-i-expose-ollama-on-my-network

# Prerequisites

- On your machine, you should be able to connect to cleps with
  `ssh cleps.inria.fr`.  This may require to be connected on the
  Inria intranet, for instance via VPN, or to have your public SSH
  key registered on `ssh-pro.inria.fr`, and having the following
  lines in your `~/.ssh/config`:
```
Host cleps.inria.fr
ProxyJump ssh-pro.inria.fr
```

- On your machine, you should have an [`ollama` client]. Note that you
  don't need to setup a server service: for instance, on Mac OS,
  `brew install ollama` is enough
  (you don't need `brew install --cask ollama`).

[`ollama` client]: https://ollama.com/download

- On cleps, `/scratch/$USER/ollama/ollama` should be the `ollama`
  executable.  You may download `ollama` with the following commands
  (to be executed on cleps):
```bash
mkdir /scratch/$USER/ollama
curl -L https://ollama.com/download/ollama-linux-amd64 -o /scratch/$USER/ollama/ollama
chmod +x /scratch/$USER/ollama/ollama
```

- On cleps, `~/.ollama/id_rsa` should be a private SSH key without
  passphrase that is authorized on cleps (you may give a path to
  another private SSH key, without passphrase, by defining the
  environment variable `SSH_KEY`).  You may create such a private SSH
  key with the following commands (to be executed on cleps):
```bash
mkdir -p ~/.ollama ~/.ssh
ssh-keygen -N "" -f ~/.ollama/id_rsa
cat ~/.ollama/id_rsa.pub >>~/.ssh/authorized_keys
```

# Usage

- Run `./ollama_on_cleps.sh` on your machine.
  You may define the following variables in your environment before:
  
  - `OLLAMA_HOST`: the bind address and the port on your local
    machine, which the client is supposed to connect to (by default,
    `127.0.0.1:11434`, which is the default for `ollama`);

  - `OLLAMA_SERVER_PORT`: the port that will be used by the server on
    the GPU node (by default, a random port will be picked, hoping it
    will be free);

  - `CLEPS_PORT`: the port that will be used on cleps to pipe
    your machine to the GPU node (by default, a random port will be
    picked, hoping it will be free);
    
  - `SSH_KEY`: the path to a passphrase-less private SSH key authorized
    to connect on cleps (by default, `~/.ollama/id_rsa`).

- In another terminal, run `ollama run zephyr` (or any other [model]).

[model]: https://ollama.com/library
