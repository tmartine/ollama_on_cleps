#!/bin/sh
set -ex
OLLAMA_HOST=${OLLAMA_HOST:=127.0.0.1:11434}
OLLAMA_SERVER_PORT=${OLLAMA_SERVER_PORT:=$(( $RANDOM + 1024 ))}
CLEPS_PORT=${CLEPS_PORT:=$(( $RANDOM + 1024 ))}
SSH_KEY=${SSH_KEY:="~/.ollama/id_rsa"} # quoted to evaluate ~ on cleps
ssh -L $OLLAMA_HOST:localhost:$CLEPS_PORT cleps.inria.fr <<EOF
set -ex
salloc --partition=gpu --gres=gpu:1
set -ex
hostname
ssh -i $SSH_KEY -fNT -R $CLEPS_PORT:localhost:$OLLAMA_SERVER_PORT cleps
cd /scratch/\$USER/ollama
TMPDIR=\$TMP_DIR OLLAMA_MODELS=\$PWD/models OLLAMA_HOST=127.0.0.1:$OLLAMA_SERVER_PORT ./ollama serve
EOF
